import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

import userController from './controllers/userController';
import filmController from './controllers/filmController';

const app = express();
const router = express.Router();

// todo export this to envrc file
const mongoURL = 'mongodb+srv://admin:pzv2020@film-review-t9zjq.mongodb.net/issues?retryWrites=true';

app.use(cors());
app.use(bodyParser.json());

mongoose.connect(mongoURL);
const connection = mongoose.connection;
connection.once('open', () => {
    console.log('MongoDB database connection established successfully!');
});

router.route('/users').get(userController.getUsers);
router.route('/users/:id').get(userController.getUserById);
router.route('/users/add').post(userController.addUser);
router.route('/users/update/:id').post(userController.updateUser);
router.route('/users/delete/:id').get(userController.deleteUser);

router.route('/films').get(filmController.getFilms);
router.route('/films/:id').get(filmController.getFilmById);
router.route('/films/add').post(filmController.addFilm);
router.route('/films/update/:id').post(filmController.updateFilm);
router.route('/films/delete/:id').get(filmController.deleteFilm);
router.route('/films/addReview/:id').post(filmController.addReview);

app.use('/', router);

app.listen(4000, () => console.log(`Express server running on port 4000`));
