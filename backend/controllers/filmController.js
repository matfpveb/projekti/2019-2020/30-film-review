import Film from '../models/Film';

module.exports.getFilms = (req, res) => {
    Film.find((err, films) => {
        if (err) {
            res.status(400).send('Failed to fetch films, error: ', err);
        }
        else {
            res.json(films);
        }
    });
};

module.exports.getFilmById = (req, res) => {
    Film.findById(req.params.id, (err, film) => {
        if (err) {
            res.status(400).send('Failed to fetch film, error: ', err);
        }
        else {
            res.json(film);
        }
    });
};

module.exports.addFilm = (req, res) => {
    let film = new Film(req.body);
    film.save()
        .then((film) => {
            res.status(200).json({'film': 'Added successfully'});
        })
        .catch((err) => {
            res.status(400).send('Failed to create new record, error: ', err);
        });
};

module.exports.updateFilm = (req, res) => {
    Film.findById(req.params.id, (err, film) => {
        if (!film)
            return new Error('Could not load document');
        else {
            const {imdbId, rank, title, fullTitle, year, image, crew, imdbRating, imdbRatingCount, reviews} = req.body;
            film.imdbId = imdbId;
            film.rank = rank;
            film.title = title;
            film.fullTitle = fullTitle;
            film.year = year;
            film.image = image;
            film.crew = crew;
            film.imdbRating = imdbRating;
            film.imdbRatingCount = imdbRatingCount;
            film.reviews = reviews;
            film.save()
                .then((film) => {
                    res.status(200).json({'film': 'Updated successfully'});
                })
                .catch((err) => {
                    res.status(400).send('Failed to update record, error: ', err);
                });
        }
    });
};

module.exports.deleteFilm = (req, res) => {
    Film.findByIdAndRemove(req.params.id, (err, film) => {
        if(err)
            res.status(400).send('Failed to delete record, error: ', err);
        else
            res.json('Removed successfully');
    });
};

module.exports.addReview = (req, res) => {
    Film.findById(req.params.id, (err, film) => {
        if(!film)
            return new Error('Could not load document');
        else {
            const review = {
                user: req.body.user,
                content: req.body.content,
                score: req.body.score
            };
            film.reviews.push(review);
            film.save()
                .then((film) => {
                    res.status(200).json({'film': 'Review added successfully'});
                })
                .catch((err) => {
                    res.status(400).send('Failed to add review, err: ' + err);
                });
        }
    });
};
