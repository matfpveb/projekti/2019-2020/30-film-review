import User from '../models/User';

module.exports.getUsers = (req, res) => {
    User.find((err, users) => {
        if(err)
            console.log(err);
        else
            res.json(users);
    });
};

module.exports.getUserById = (req, res) => {
    User.findById(req.params.id, (err, user) => {
        if(err)
            console.log(err);
        else
            res.json(user);
    });
};

module.exports.addUser = (req, res) => {
    let user = new User(req.body);
    user.save()
        .then((user) => {
            res.status(200).json({'user': 'Added successfully'});
        })
        .catch((err) => {
            res.status(400).send('Failed to create new record');
        });
};

module.exports.updateUser = (req, res) => {
    User.findById(req.params.id, (err, user) => {
        if(!user)
            return new Error('Could not load document');
        else{
            user.firstName = req.body.firstName;
            user.lastName = req.body.lastName;
            user.save()
                .then((user) => {
                    res.status(200).json({'user': 'Updated successfully'});
                })
                .catch((err) => {
                    res.status(400).send('Failed to update record');
                });
        }
    });
};

module.exports.deleteUser = (req, res) => {
    User.findByIdAndRemove(req.params.id, (err, user) => {
        if(err)
            console.log(err);
        else 
            res.json('Removed successfully');
    });
};