import mongoose from 'mongoose';

const Film = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    imdbId: {
        type: String,
        required: true
    },
    rank: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    fullTitle: {
        type: String,
        required: true
    },
    year: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    crew: {
        type: String,
        required: true
    },
    imdbRating: {
        type: String,
        required: true
    },
    imdbRatingCount: {
        type: String,
        required: true
    },
    reviews: [{
        user: {
            type: String
        },
        content: {
            type: String
        },
        score: {
            type: Number
        }
    }]
});

export default mongoose.model('Film', Film);
