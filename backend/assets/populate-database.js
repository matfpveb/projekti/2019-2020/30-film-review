const moviesJSON = require('./movies');
const fetch = require('node-fetch');

async function postMovieData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        mode: 'no-cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(data)
    });
    return response.json();
}

const movies = moviesJSON.items;

movies.forEach(movie => {
    postMovieData('http://localhost:4000/films/add', movie)
        .then(data => {
            // response from server
            console.log(data);
        });
});
