export interface UserModel {
    _id: string;
    firstName: string;
    lastName: string;
}
