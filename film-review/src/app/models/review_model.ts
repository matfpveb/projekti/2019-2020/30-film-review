import { UserModel } from './user_model';

export interface ReviewModel {
    //user: UserModel;
    user: string;
    content: string;
    score: number;
}