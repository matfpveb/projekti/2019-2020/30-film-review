import { ReviewModel } from './review_model';

export interface FilmModel {
  _id: string;
  imdbId: string;
  rank: string;
  title: string;
  fullTitle: string;
  year: string;
  image: string;
  crew: string;
  imdbRating: string;
  imdbRatingCount: string;
  reviews: ReviewModel[];
}
