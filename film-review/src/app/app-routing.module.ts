import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddReviewComponent} from './components/add-review/add-review.component';
import {FilmsListComponent} from './components/films-list/films-list.component';
import {ListReviewsComponent} from './components/list-reviews/list-reviews.component';

const routes: Routes = [
  { path: 'films', component: FilmsListComponent },
  { path: 'films/:id', component: ListReviewsComponent },
  //{ path: 'films/addReview/:id', component: AddReviewComponent },
  { path: '', redirectTo: '/films', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
