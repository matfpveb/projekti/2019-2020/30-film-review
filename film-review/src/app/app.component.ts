import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';

import {FilmsService} from './services/films.service';
import {FilmModel} from './models/film_model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'film-review';
  selectedMovie: FilmModel;
  private activeSubs: Subscription[] = [];

  // search
  myControl = new FormControl();
  options: FilmModel[] = [];
  filteredOptions: FilmModel[] = [];

  constructor(private filmsService: FilmsService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    const sub = this.filmsService.getFilms().subscribe((films: FilmModel[]) => {
      this.options = films;
      this.filteredOptions = films;

      this.myControl.valueChanges.subscribe(value => {
        if (typeof value === 'string') {
          this.filteredOptions = this._filter(value);
        }
      });
    });
    this.activeSubs.push(sub);
  }

  displayFn(movie: FilmModel): string {
    return movie && movie.title ? movie.title : '';
  }

  private _filter(title: string): FilmModel[] {
    const filterValue = title.toLowerCase();

    return this.options.filter(option => option.title.toLowerCase().indexOf(filterValue) === 0);
  }

  onMovieSelected(event: MatAutocompleteSelectedEvent) {
    console.log('event: option selected is ', event.option.value);
    this.selectedMovie = event.option.value;
    if (this.selectedMovie) {
      this.router.navigate(['/films/' + this.selectedMovie._id], { relativeTo: this.route });
    }
  }

  ngOnDestroy() {
    this.activeSubs.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
