import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { ReviewModel } from '../../models/review_model';
import { FilmsService } from '../../services/films.service';
import { FilmModel } from '../../models/film_model';

@Component({
  selector: 'app-list-reviews',
  templateUrl: './list-reviews.component.html',
  styleUrls: ['./list-reviews.component.scss']
})
export class ListReviewsComponent implements OnInit, OnDestroy {
  reviews: ReviewModel[] = [];
  private activeSubs: Subscription[] = [];
  public film: FilmModel;

  constructor(private route: ActivatedRoute, private filmsService: FilmsService) {
    const sub = this.route.paramMap
      .pipe(
        map((paramMap) => paramMap.get('id')),
        switchMap((id) => this.filmsService.getFilmById(id))
      )
      .subscribe((film) => {
        this.reviews = film.reviews;
        this.film = film;
        console.log('reviews: ', this.reviews);
      });
    this.activeSubs.push(sub);
  }

  ngOnInit(): void {

  }

  ngOnDestroy() {
    this.activeSubs.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
