import {Component, OnDestroy, OnInit} from '@angular/core';
import { Subscription } from 'rxjs';
import { FilmsService } from '../../services/films.service';
import { FilmModel } from '../../models/film_model';

@Component({
  selector: 'app-films-list',
  templateUrl: './films-list.component.html',
  styleUrls: ['./films-list.component.scss']
})
export class FilmsListComponent implements OnInit, OnDestroy {
  films: FilmModel[] = [];
  filmsLoaded = false;
  private activeSubs: Subscription[] = [];

  constructor(private filmsService: FilmsService) {

  }

  ngOnInit() {
    const sub = this.filmsService.getFilms().subscribe((films: FilmModel[]) => {
      console.log('fetched films: ', films);
      this.films = films;
      this.filmsLoaded = true;
    });
    this.activeSubs.push(sub);
  }

  ngOnDestroy() {
    this.activeSubs.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
