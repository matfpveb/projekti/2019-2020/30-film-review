import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

import { FilmModel } from 'src/app/models/film_model';
import { FilmsService } from 'src/app/services/films.service';

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.scss']
})
export class AddReviewComponent implements OnInit, OnDestroy {
  public addReviewForm: FormGroup;
  private activeSubs: Subscription[] = [];
  @Input('filmId')
  public filmId: string;
  public totalRating = 0;
  public totalSum = 0;

  constructor(private filmsService: FilmsService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
    this.addReviewForm = this.formBuilder.group({
      username: ['', [ Validators.required ]],
      content: ['', [ Validators.required ]],
      score: ['', [Validators.min(0), Validators.max(10)]]
    });
  }

  ngOnInit(): void {
    this.totalRating = this.calculateRating();
  }

  ngOnDestroy() {
    this.activeSubs.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  addReview(data) {
    if (!this.addReviewForm.valid) {
      window.alert('The form of review is not valid!');
      return Error.toString();
    }
    console.log(this.filmId);
    const body = {
      id: this.filmId,
      username: data.username,
      content: data.content,
      score: data.score
    };
    const subReview = this.filmsService.addReview(body).subscribe(
    (film: FilmModel) => {
      this.openSnackBar('Your review was successfully submitted!', '');
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }
    );
    this.activeSubs.push(subReview);
  }

  calculateRating(): number {
    const sub = this.filmsService.getFilmById(this.filmId).subscribe((film) => {
      film.reviews.forEach((review) => {
        this.totalSum += review.score;
      });
      this.totalRating = (this.totalSum / film.reviews.length);
      this.totalSum = 0;
     });
    this.activeSubs.push(sub);
    return this.totalRating;
  }
}

