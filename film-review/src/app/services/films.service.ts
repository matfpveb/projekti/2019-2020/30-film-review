import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { FilmModel } from '../models/film_model';
import { ReviewModel } from '../models/review_model';
import { UserModel } from '../models/user_model';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {
  uri = 'http://localhost:4000';

  constructor(private http: HttpClient) {

  }

  getFilms(): Observable<FilmModel[]> {
    return this.http.get<FilmModel[]>(`${this.uri}/films`);
  }

  getFilmById(id): Observable<FilmModel> {
    return this.http.get<FilmModel>(`${this.uri}/films/${id}`);
  }

  addFilm(id, imdbId, rank, title, fullTitle, year, image, crew, imdbRating, imdbRatingCount): Observable<FilmModel> {
    const film = {
      id,
      imdbId,
      rank,
      title,
      fullTitle,
      year,
      image,
      crew,
      imdbRating,
      imdbRatingCount,
    };
    return this.http.post<FilmModel>(`${this.uri}/films/add`, film);
  }

  updateFilm(id, imdbId, rank, title, fullTitle, year, image, crew, imdbRating, imdbRatingCount): Observable<FilmModel> {
    const film = {
      id,
      imdbId,
      rank,
      title,
      fullTitle,
      year,
      image,
      crew,
      imdbRating,
      imdbRatingCount,
    };
    return this.http.post<FilmModel>(`${this.uri}/films/update/${id}`, film);
  }

  deleteFilm(id): Observable<FilmModel> {
    return this.http.get<FilmModel>(`${this.uri}/films/delete/${id}`);
  }

  addReview(body): Observable<FilmModel> {
    const id = body.id;
    const review: ReviewModel = {user: body.username, content: body.content, score: body.score};
    return this.http.post<FilmModel>(`${this.uri}/films/addReview/${id}`, review);
  }

  getUserById(id): Observable<UserModel> {
    return this.http.get<UserModel>(`${this.uri}/users/${id}`);
  }

  addNewUser(firstName, lastName): Observable<UserModel> {
    return this.http.post<UserModel>(`${this.uri}/users/add`, {firstName, lastName});
  }
}
