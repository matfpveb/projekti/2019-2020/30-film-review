# FilmReview

## Development
Run frontend with command `npm run start`

## Backend
Run backend server with command `npm run dev`

## DB
Run db in MongoDB Compass with connection url:
`mongodb+srv://admin:pzv2020@film-review-t9zjq.mongodb.net/issues?retryWrites=true`
