# Project 30-Film-Review

Aplikacija za citanje i postavljanje recenzija za filmove. Korisnik ce moci da otvori stranicu sa filmom, procita recenzije i doda svoju. Filmovi ce biti sortirani u kategorije. Potencijalno ce postojati i forumski deo, za svaki film pojedinacno ili objedinjeni

## Development
Run frontend with command `npm run start`

## Backend
Run backend server with command `npm run dev`

## DB
Run db in MongoDB Compass with connection url:
`mongodb+srv://admin:pzv2020@film-review-t9zjq.mongodb.net/issues?retryWrites=true`


## Developers

- [Anja Miletic, 97/2015](https://gitlab.com/anyamiletic)
- [Tatjana Radovanovic, 1103/2018](https://gitlab.com/Tatjana95)
- [Marina Borozan, 1092/2018](https://gitlab.com/marinaborozan)
